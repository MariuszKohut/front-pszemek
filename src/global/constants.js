// Used for saga states
export const FULFILLED = '_FULFILLED';
export const REJECTED = '_REJECTED';
export const INVALID_VALIDATION = '_INVALID_VALIDATION';

// Check error response if it´s unauthorized response
export const CHECK_UNAUTHORIZED = 'CHECK_UNAUTHORIZED';