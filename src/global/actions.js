/**
 * Rejected state for SAGA action calls
 * @param _type
 * @param response
 * @returns {{type: *, response: *}}
 */
import { CHECK_UNAUTHORIZED, FULFILLED, INVALID_VALIDATION, REJECTED } from './constants';

export const rejected = (_type, response) => {
    return {
        type: _type + REJECTED,
        response
    }
};

/**
 * Fulfilled state for SAGA action calls
 * @param _type
 * @param response
 * @returns {{type: *, response: *}}
 */
export const fulfilled = (_type, response = {}) => {
    return {
        type: _type + FULFILLED,
        response
    }
};


/**
 * Invalid state for SAGA action calls - used when validation has errors
 * @param _type
 * @param errors
 * @returns {{type: *, response: *}}
 */
export const invalid = (_type, errors = {}) => {
    return {
        type: _type + INVALID_VALIDATION,
        errors
    }
};


/**
 * Check rejected response for unauthorized access
 * @param error
 * @returns {{type, error: *}}
 */
export const checkUnauthorized = (error) => {
  return {
      type: CHECK_UNAUTHORIZED,
      error
  }
};