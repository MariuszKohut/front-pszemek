import React, { PureComponent } from 'react';
import { BrowserRouter as Router, Route, Switch, Redirect, withRouter, Link } from 'react-router-dom';
import { Provider } from 'react-redux';
import { url, get, post, del, update, setToken, getToken } from './network';
// Containers
import IntroductionContainer from './containers/IntroductionContainer';
import UsersContainer from './containers/UsersContainer';
import Login,{ SecuredRoute } from './containers/Login';

export const routes = store => {
  return (
    <Provider store={store}>
      <Router>
        <Switch>
          {/* THERE WILL BE ALL YOUR ROUTES */}
          <Route path="/login" component={Login}/>
          <Route exact path="/" component={IntroductionContainer} />
          <SecuredRoute path="/user" component={UsersContainer} />
        </Switch>
      </Router>
    </Provider>
  );
};
