import cookie from 'react-cookie';

/**
 * @function Network
 * @description Factory function to create a object that can send
 * requests to a specific resource on the server.
 * @param {string} resource The resource used for config
 */
export const hosts = {
  development: {
    auth: 'http://localhost:8081/auth/ouath',
    core: 'http://localhost:8081/auth',
  },
  test: {
    auth: 'http://localhost:8081/auth/ouath',
    core: 'http://localhost:8081/auth',
  },
  production: {
    auth: 'http://localhost:8081/auth/ouath',
    core: 'http://localhost:8081/auth',
  },
};

/**
 * Get host based on environment
 */
export const getHost = module => {
  const env = process.env.REACT_APP_ENV.trim();

  if (!env) {
    console.log('Environment is undefined');
    return;
  }

  return hosts[env][module];
};

/**
 * Tokens stored in cookies
 */
export const getToken = () => cookie.load('token');

/**
 * Check if cookies contains token
 * @returns {boolean}
 */
export const hasToken = () => {
  return !!(getToken() && getToken().access_token);
};

/**
 * Save token into cookies
 * @param token
 * @returns {*}
 */
export const setToken = token => {
  return cookie.save('token', token, cookiePath);
};

/**
 * Remove token from cookies
 * @returns {*}
 */
export const removeToken = () => {
  return cookie.remove('token', cookiePath);
};

/**
 * Get refresh token body
 * @returns {{grant_type: string, refresh_token: *}}
 */
export const getRefreshTokenBody = () => {
  const token = getToken();
  return {
    grant_type: 'refresh_token',
    refresh_token: token.refresh_token,
  };
};

/**
 * Get default headers
 * @param customHeader
 * @returns {{}}
 */
export const getHeader = (customHeader = {}) => {
  let defaultHeader = {
    'Authorization': 'Bearer ' + getToken(),
    // Authorization: hasToken()
    //   ? `Bearer ${getToken().access_token}`
    //   : 'Bearer 2dd4177f-3632-4120-a30f-98cb08ef12a2',
    ac: 'application/json',
    'cache-control': 'no-cache',
    // 'Content-Type': 'application/x-www-form-urlencoded', //
  };

  let newHeaders = { ...defaultHeader, ...customHeader };

  // Fix for multipart data - FormData
  if (newHeaders['Content-Type'] === null) {
    delete newHeaders['Content-Type'];
  }

  // Default options used for every request are overwritten by customHeader if any
  return newHeaders;
};

/**
 * Get full URL - merge host + url part
 * @param url
 * @param module
 * @returns {string}
 */
export const url = (url, module = 'core') => {
  const hostUrl = getHost(module);

  if (hostUrl) {
    return `${hostUrl}/${url}`;
  } else {
    console.error(`Host "${module}" doesn´t exists`);
  }
};

/**
 * Request - POST
 * @param customHeaders
 * @param body
 * @returns {{method: string, headers: {}, body: *}}
 */
export const post = (customHeaders = {}, customBody = {}) => {
  return {
    method: 'POST',
    headers: getHeader(customHeaders),
    body: customBody,
  };
};

/**
 * Request - GET
 * @param customHeaders
 * @returns {{method: string, headers: {}}}
 */
export const get = (customHeaders = {}, customBody = {}) => {
  return {
    method: 'GET',
    headers: getHeader(customHeaders),
  };
};

/**
 * Request - DELETE
 * @param customHeaders
 * @param body
 * @returns {{method: string, headers: {}, body: {}}}
 */
export const del = (customHeaders = {}, body = {}) => {
  return {
    method: 'DELETE',
    headers: getHeader(),
    body,
  };
};

/**
 * Request - UPDATE
 * @param customHeaders
 * @param body
 * @returns {{method: string, headers: {}, body: {}}}
 */
export const update = (customHeaders = {}, body = {}) => {
  return {
    method: 'UPDATE',
    headers: getHeader(),
    body,
  };
};

/**
 * Export cookie path
 * @type {{path: string}}
 */
export const cookiePath = {
  path: '/',
};
