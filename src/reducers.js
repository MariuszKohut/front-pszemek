import introductionContainer from './containers/IntroductionContainer/reducer';
import usersContainer from './containers/UsersContainer/reducer';
import loginContainer from './containers/Login/reducer';


export default {
  // There will be all your container´s reducers
  // 1. Import reducer ex. import SomeReducer from './containers/SomeContainer/reducer'
  // 2. Include imported reducer in this object
  introductionContainer,
  usersContainer,
  loginContainer
}
