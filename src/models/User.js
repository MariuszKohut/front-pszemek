/*
 *
 * User model
 *
 */
import { Map } from 'immutable';

// User - Constants
export const cUser = {
  userId: 'userId',
  username: 'username',
};

// User - Model
export const User = new Map({
  [cUser.authenticated]: false,
  [cUser.name]: '',

});

// User - Model Mapper
export const mapUser = src => new Map({
  [cUser.userId]: src.userId,
  [cUser.username]: src.username,
});

// User - Model - List to Map Mapper
export const mapUserListToMap = src => {
  let map = new Map();

  src.map(o => {
    const obj = mapUser(o);
    map = map.set(obj.get(cUser.userId), obj);
    return obj;
  });

  return map;
};

export const cNote = {
  noteId: 'noteId',
  tag: 'tag',
  label: 'label',
  owner: 'owner',
  notes: 'notes',
};

export const mapNote = src => new Map({
  [cNote.noteId]: src.id,
  [cNote.tag]: src.tag,
  [cNote.label]: src.label,
  [cNote.owner]: src.owner,
  [cNote.notes]: src.notes,
});

export const mapNotes = src => {
  let map = new Map();

  src.map(o => {
    console.log(o);
    const obj = mapNote(o);
    map = map.set(obj.get(cNote.noteId), obj);
    return obj;
  });

  return map;
};
