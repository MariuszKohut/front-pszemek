/*
 *
 * IntroductionPost model
 *
 */
import { Map } from 'immutable';

// NOTE: IntroductionPost - Constants
//
// In our code we use constants instead of strings. Why?
// When we refactor our code constants are simple and there are no errors with it.
// String are hard to refactor because there is no reference between them so due refactoring
// reasons we are referencing instead of hard typing strings.
export const cIntroductionPost = {
  id: 'id',
  userId: 'userId',
  title: 'title',
  body: 'body',
};

// NOTE: IntroductionPost - Model
//
// Each model is immutable Map ( check out immutable.js )
// You may see that we are using reference in keys instead of strings
export const IntroductionPost = new Map({
  [cIntroductionPost.id]: 0,
  [cIntroductionPost.userId]: 0,
  [cIntroductionPost.title]: '',
  [cIntroductionPost.body]: '',
});

// NOTE: IntroductionPost - Model Mapper
//
// This is mapper function used for mapping the response.
// Each time we make request for a data from API we want to map the response to correct model.
export const mapIntroductionPost = src => new Map({
  [cIntroductionPost.id]: src[cIntroductionPost.id],
  [cIntroductionPost.userId]: src[cIntroductionPost.userId],
  [cIntroductionPost.title]: src[cIntroductionPost.title],
  [cIntroductionPost.body]: src[cIntroductionPost.body],
});

// NOTE: IntroductionPost - Model - List to Map Mapper
//
// You already know Arrays, Immutable.js has Lists.
// Lists were used for containing list of items. However now we are using Maps instead. Why?
// With maps you may have as a key what ever you want, ex. id of the item. With this approach
// you may use a quick get/set functions and change item content based on its ID.
// With lists its not possible and you have to iterate them. This function will remap array/list to
// map with key of item ID. You may then quickly access the item like: someReducer.getIn(['usersMap', 'itemID'])
export const mapIntroductionPostListToMap = src => {
  let map = new Map();

  src.map(o => {
    const obj = mapIntroductionPost(o);
    map = map.set(obj.get(cIntroductionPost.id), obj);
    return obj;
  });

  return map;
};