/**
*
* Introduction
*
*/

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import logo from './logo.svg';
import './style.css';

class Introduction extends PureComponent {
  // eslint-disable-line react/prefer-stateless-function

  renderHeader() {
    return (
      <header className="text-center app-header py-4">
        <img src={logo} className="app-logo" alt="logo" />
      </header>
    );
  }

  renderUsedTechnologies() {
    return null;
  }

  renderIntro() {
    return (
      <div className="py-5">
        <div className="jumbotron text-center">
          <h1 className="display-4">Welcome Padavan!</h1>
          <p className="lead">
            You have made a successful setup of your project!
          </p>
        </div>
      </div>
    );
  }

  renderIntroDescription() {
    const texts = [
      {
        title: 'Do you React?',
        desc: 'With this template you will learn all basics and a little bit advanced topics of React. Don´t worry, we will guide and help you ( a little bit ).',
      },
      {
        title: 'No configuration',
        desc: 'We already prepared the whole project for you. You will be only coding features and doing no configuration at all. Everything is prepared and configured for you.',
      },
      {
        title: 'Generators will help you!',
        desc: 'The project contains generators which will generate all necessary parts of code for you. You have no idea how to write components? Generators will do this for you.',
      },
    ];
    const _texts = texts.map((text, i) => {
      return (
        <div className="col-md-4" key={i}>
          <h3>{text.title}</h3>
          <p>{text.desc}</p>
        </div>
      );
    });

    return (
      <div className="mb-5">
        <div className="row">{_texts}</div>
        <hr />
      </div>
    );
  }

  renderUsedDeveloperTools() {
    const tools = [
      {
        title: 'React Developer Tools',
        desc: 'Inspect React components inside of your Chrome Developer Tools',
        link: 'https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi',
      },
      {
        title: 'Redux DevTools',
        desc: 'See what actions are made in your application and application state changes.',
        link: 'https://chrome.google.com/webstore/detail/redux-devtools/lmhkpmbekcpmknklioeibfkpmmfibljd',
      },
      {
        title: 'Installation done?',
        desc: 'Inspect the page and select Redux/React tab to see how it works. We have prepared for you some async fetch example which you can see in Redux tab..',
      },
    ];
    const _tools = tools.map((tech, i) => {
      return (
        <div className="col-md-4" key={i}>
          <div className="card mb-4">
            <div className="card-body">
              <h5 className="card-title">{tech.title}</h5>
              <p className="card-text">{tech.desc}</p>
              {tech.link ? <a href={tech.link} className="btn btn-primary">
                Learn More
              </a> : null}
            </div>
          </div>
        </div>
      );
    });

    return (
      <div className="mb-5">
        <div className="mb-5">
          <h3>Used developer tools</h3>
          <p>
            There is a lot of useful tools which will help you on your journey. It´s recommended to install and use them all. They will make your React developer life easier than you think.
          </p>
        </div>
        <div className="row">{_tools}</div>
      </div>
    );
  }

  renderUsedTechnologies() {
    const technologies = [
      {
        title: 'Immutable.js',
        desc: 'Immutable data structures like Lists, Maps, Record etc.',
        link: 'https://facebook.github.io/immutable-js/',
      },
      {
        title: 'Bootstrap v4',
        desc: 'CSS Framework used for design of application.',
        link: 'https://getbootstrap.com/',
      },
      {
        title: 'React Router v4',
        desc: 'Simple router used for navigation between pages, redirects etc.',
        link: 'https://reacttraining.com/react-router/',
      },
      {
        title: 'Redux',
        desc: 'Predictable state container used for managing state of application.',
        link: 'https://redux.js.org/introduction/motivation',
      },
      {
        title: 'Redux-Saga',
        desc: 'Simple handling of async communication with control over side effects.',
        link: 'https://redux-saga.js.org/',
      },
      {
        title: 'Validate.js',
        desc: 'Simple and efficient way of validating javascript objects.',
        link: 'https://validatejs.org/',
      },
      {
        title: 'Moment.js',
        desc: 'Parse, validate, manipulate, and display dates and times in JavaScript.',
        link: 'https://momentjs.com/',
      },
    ];
    const _technologies = technologies.map((tech, i) => {
      return (
        <div className="col-md-4" key={i}>
          <div className="card mb-4">
            <div className="card-body">
              <h5 className="card-title">{tech.title}</h5>
              <p className="card-text">{tech.desc}</p>
              <a href={tech.link} className="btn btn-primary">
                Learn More
              </a>
            </div>
          </div>
        </div>
      );
    });

    return (
      <div className="mb-5">
        <div className="mb-5">
          <h3>Used Technologies</h3>
          <p>
            There is a lot of technologies which we use with React. You should check some of them because they will help you build your project.
          </p>
        </div>
        <div className="row">{_technologies}</div>
      </div>
    );
  }

  renderCheatSheets() {
    const cheatSheets = [
      {
        title: 'React CheatSheet',
        desc: 'CheatSheet for React made by Egghead',
        link: 'https://drive.google.com/file/d/0B0S4GyTh4nw5UHlRUHNqanhuR1k/view',
      },
      {
        title: 'Redux CheatSheet',
        desc: 'CheatSheet for Redux made by Egghead.',
        link: 'https://drive.google.com/file/d/0B0S4GyTh4nw5OTZXd2ZyQnVXeTg/view',
      },
    ];
    const _cheatSheets = cheatSheets.map((sheet, i) => {
      return (
        <div className="col-md-4" key={i}>
          <div className="card mb-4">
            <div className="card-body">
              <h5 className="card-title">{sheet.title}</h5>
              <p className="card-text">{sheet.desc}</p>
              <a href={sheet.link} className="btn btn-primary">
                Learn More
              </a>
            </div>
          </div>
        </div>
      );
    });

    return (
      <div className="mb-5">
        <div className="mb-5">
          <h3>CheatSheets</h3>
          <p>
            Even after few months of working with React you will forget something. These cheat sheets will always help you when you forget something.
          </p>
        </div>
        <div className="row">{_cheatSheets}</div>
      </div>
    );
  }

  renderFooter() {
    return (
      <div className="text-center mb-5">
        <h3>Good luck! Let be force with you!</h3>
      </div>
    );
  }

  render() {
    return (
      <div>
        {this.renderHeader()}
        <div className="container">
          {this.renderIntro()}
          {this.renderIntroDescription()}
          {this.renderUsedDeveloperTools()}
          {this.renderUsedTechnologies()}
          {this.renderCheatSheets()}
          {this.renderFooter()}
        </div>
      </div>
    );
  }
}

Introduction.propTypes = {};

export default Introduction;
