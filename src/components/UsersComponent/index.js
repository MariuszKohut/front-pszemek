/**
*
* UsersComponent
*
*/

import React from 'react';
import PropTypes from 'prop-types';

const UsersComponent = (props) => (

  <div>
   <div>id: {props.id}</div>
   <div>tag:{props.tag} </div>
   <div>label:{props.label} </div>
   <div>owner:{props.owner.username} </div>
  </div>
)

UsersComponent.propTypes = {

};

export default UsersComponent;
