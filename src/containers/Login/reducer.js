/*
 *
 * UsersContainer reducer
 *
 */

import { Map } from 'immutable';
import {
  cReducer as c,
  DEFAULT_ACTION,
  LOGIN,
} from './constants';
import { FULFILLED, REJECTED } from '../../global/constants';

const initialState = Map({
  isAuthenticated: false,
  redirectToPreviousRoute: false,
});

function loginReducer(state = initialState, action) {
  console.log(action.type);
  switch (action.type) {
    case `${LOGIN}`:
      return state.set('loading', true);
    case LOGIN+'_REJECTED':
      return state
      .set('loading', false)
      .set('isAuthenticated', true)
      .set('redirectToPreviousRoute', true);
    case `${LOGIN}${FULFILLED}`:
      // Here we map our array of items in response to Map
      // Where each key in map will be same as item ID
      console.log('login fulfilled')
      return state.set('loading', false).set('isAuthenticated', true).set('redirectToPreviousRoute', true);
    default:
      return state;
  }
}

export default loginReducer;
