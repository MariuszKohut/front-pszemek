/*
 *
 * UsersContainer constants
 *
 */

export const DEFAULT_ACTION = 'Login/DEFAULT_ACTION';

export const cReducer = {
  loading: 'loading',
  items: 'items',
  user: 'user',
};

export const LOGIN = 'Login/LOGIN';
