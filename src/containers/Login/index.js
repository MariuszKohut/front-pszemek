/*
 *
 * Login
 *
 */

 import React, { PureComponent } from 'react';
 import PropTypes from 'prop-types';
 import { connect } from 'react-redux';
 import { cReducer as c } from './constants';
 import * as actions from './actions';
 import { bindActionCreators } from 'redux';
import { BrowserRouter as Router, Route, Switch, Redirect, withRouter, Link } from 'react-router-dom';

class Login extends PureComponent { // eslint-disable-line react/prefer-stateless-function

    // componentWillMount() {
    //   this.props.actions.login();
    // }

    onSubmit = (event) => {
      event.preventDefault();
      const data = new FormData(event.target);
      this.props.actions.login(data);
      this.props.history.push("/user");
    }


    render() {
      if (this.props.isAuthenticated) {
        this.props.history.push("/user");
      }
      return (
        <div style={{ textAlign: 'center' }}>
          <form onSubmit={this.onSubmit}>
             <div>
               <input id='username' name='username' type='text' />
             </div>
             <div>
               <input id='password' name='password' type='password' />
             </div>
             <div style={{ textAlign: 'center' }}>
              <button type="submit">Log in</button>
             </div>
           </form>
        </div>
      );
    }
  }

const AuthService = () => {
  isAuthenticated: this.props.isAuthenticated;
};

export function SecuredRoute(props) {
  const {component: Component, path} = props;
  return (
    <Route path={path} render={() => {
        if (!AuthService.isAuthenticated) <Redirect to='/login' />;
        return <Component />
    }} />
  );
}

const AuthStatus = withRouter(({ history }) => (
  AuthService.isAuthenticated ? (
    <p>
      Welcome! <button onClick={() => {
        AuthService.logout(() => history.push('/'))
      }}>Sign out</button>
    </p>
  ) : (
    <p>You are not logged in.</p>
  )
));

Login.propTypes = {
  //
};


const mapStateToProps = (state) => { return {
  loginContainer: state.loginContainer,
  isAuthenticated: state.loginContainer.get('isAuthenticated'),
  redirectToPreviousRoute: state.loginContainer.get('redirectToPreviousRoute'),
}};

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);
