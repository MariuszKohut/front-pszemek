/*
 *
 * UsersContainer actions
 *
 */

import {
  DEFAULT_ACTION,
  LOGIN,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

export function login(data) {
  return {
    type: LOGIN,
    payload: data,
  };
}
