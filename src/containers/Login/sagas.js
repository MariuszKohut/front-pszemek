/*
*
* UsersContainer saga
*
*/

import { take, call, put, select, takeLatest, takeEvery, all, fork } from 'redux-saga/effects';
import { LOGIN } from './constants';
import { url, get, post, del, update, setToken, getToken } from '../../network';
import request from '../../request';
import { fulfilled, rejected } from '../../global/actions';
import * as actions from './actions';
import URLSearchParams from 'url-search-params';
import {
  cReducer as c,

} from './constants';
// Individual exports for testing
export function* login(action) {
  try {
    // WARNING:
    // This is hard-coded url for introduction purposes only
    // For your purposes there is method url() which you will use for getting data from your backend
    // In your case your urls will look like: url('/users')
    // You may found more in network.js file, you may configure headers here etc.
    const password = action.payload.get('password');
    const username = action.payload.get('username');
    const url = 'http://localhost:8081/auth/oauth/token';
    const searchParams = new URLSearchParams();
      searchParams.set('grant_type', 'password');
      searchParams.set('username', username);
      searchParams.set('password', password);
    const response = yield call(request, url, fetch(url, {
      method: 'post',
      headers: new Headers({
        'Accept': 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'Basic ' + btoa('frontendClientId:frontendClientSecret'),
      }),
      body:  searchParams,
    }).then(response => response.json()).then(data => {
      console.log(data.access_token);
      setToken(data.access_token);
    }));

    yield put(fulfilled(LOGIN, response));
  } catch (err) {
    yield put(rejected(LOGIN, err));
  }
}


// All sagas to be loaded
export default function* sagas() {
  yield all([
    yield takeLatest(LOGIN, action => login(action)),
  ]);
}
