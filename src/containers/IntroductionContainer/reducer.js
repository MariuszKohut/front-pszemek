/*
 *
 * IntroductionContainer reducer
 *
 */

import { Map } from 'immutable';
import {
  cReducer as c,
  FETCH_FAKE_DATA,
} from './constants';
import { FULFILLED, REJECTED } from '../../global/constants';
import { mapIntroductionPostListToMap } from '../../models/IntroductionPost';

const initialState = Map({
  [c.loading]: true,
  [c.items]: new Map(),
});

function reducer(state = initialState, action) {
  switch (action.type) {
    case `${FETCH_FAKE_DATA}`:
      return state.set('loading', true);
    case `${FETCH_FAKE_DATA}${FULFILLED}`:
      // Here we map our array of items in response to Map
      // Where each key in map will be same as item ID
      const items = mapIntroductionPostListToMap(action.response);
      return state.set('loading', false).set(c.items, items);
    case `${FETCH_FAKE_DATA}${REJECTED}`:
      return state.set('loading', false);
    default:
      return state;
  }
}

export default reducer;
