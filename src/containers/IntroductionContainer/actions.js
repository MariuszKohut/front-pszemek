/*
 *
 * IntroductionContainer actions
 *
 */

import { FETCH_FAKE_DATA } from './constants';

export function fetchFakeData() {
  return {
    type: FETCH_FAKE_DATA,
  };
}
