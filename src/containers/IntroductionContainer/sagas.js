/*
*
* IntroductionContainer saga
*
*/

import { take, call, put, select, takeLatest, all } from 'redux-saga/effects';
import { FETCH_FAKE_DATA } from './constants';
import { url, get, post, del, update } from '../../network';
import request from '../../request';
import { fulfilled, rejected } from '../../global/actions';

// Individual exports for testing
export function* fetchFakeData(action) {
  try {
    // WARNING:
    // This is hard-coded url for introduction purposes only
    // For your purposes there is method url() which you will use for getting data from your backend
    // In your case your urls will look like: url('/users')
    // You may found more in network.js file, you may configure headers here etc.
    const url = 'https://jsonplaceholder.typicode.com/posts';

    const response = yield call(request, url, get());
    yield put(fulfilled(FETCH_FAKE_DATA, response));
  } catch (err) {
    yield put(rejected(FETCH_FAKE_DATA, err));
  }
}

// All sagas to be loaded
export default function* sagas() {
  yield all([
    yield takeLatest(FETCH_FAKE_DATA, action => fetchFakeData(action)),
  ]);
}
