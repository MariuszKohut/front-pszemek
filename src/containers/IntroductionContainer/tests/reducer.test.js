
import { fromJS } from 'immutable';
import introductionContainerReducer from '../reducer';

describe('introductionContainerReducer', () => {
  it('returns the initial state', () => {
    expect(introductionContainerReducer(undefined, {})).toEqual(fromJS({}));
  });
});
