/*
 *
 * IntroductionContainer constants
 *
 */

export const FETCH_FAKE_DATA = 'IntroductionContainer/FETCH_FAKE_DATA';

export const cReducer = {
  loading: 'loading',
  items: 'items',
};
