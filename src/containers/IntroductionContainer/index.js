/*
 *
 * IntroductionContainer
 *
 */

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { cReducer as c } from './constants';
import * as actions from './actions';
import { bindActionCreators } from 'redux';
import Introduction from '../../components/Introduction/index';

class IntroductionContainer extends PureComponent { // eslint-disable-line react/prefer-stateless-function

  componentWillMount() {
    this.props.actions.fetchFakeData();
  }

  render() {
    return (
      <Introduction/>
    );
  }
}

IntroductionContainer.propTypes = {
  //
};

const mapStateToProps = (state) => ({
  introductionContainer: state.introductionContainer,
  [c.fakeData]: state.introductionContainer.get(c.fakeData)
});

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(IntroductionContainer);
