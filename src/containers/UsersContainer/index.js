/*
 *
 * UsersContainer
 *
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { cReducer as c } from './constants';
import * as actions from './actions';
import { bindActionCreators } from 'redux';
import User from '../../components/UsersComponent/index';

class UsersContainer extends Component { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
      super(props);
  }

  componentWillMount() {
    this.props.actions.fetchUser();
  }

  onSubmit = (event) => {
    event.preventDefault();
    const data = new FormData(event.target);
    this.props.actions.saveUser(data);
  }

  render() {
    return (
      <div>
      {this.props.user.map(user => (
        console.log('user' + user),
        <div>
          <User key={user.get('noteId')}
            id={user.get('noteId')}
            tag={user.get('tag')}
            label={user.get('label')}
            owner={user.get('owner')}


            />
            {user.get('notes').map(notes => (
              <div>
                <div>Title: {notes.title}</div>
                <div>Content: {notes.content}</div>
              </div>
            ))}
            </div>
        ))
      }
      <div style={{ textAlign: 'center' }}>
        <form onSubmit={this.onSubmit}>
           <div>
             <input id='username' name='username' type='text' />
           </div>
           <div>
             <input id='password' name='password' type='password' />
           </div>
           <div style={{ textAlign: 'center' }}>
            <button type="submit">Add new user</button>
           </div>
         </form>
      </div>
    </div>);
  }

}
UsersContainer.propTypes = {
  //
};


const mapStateToProps = (state) => { return {
  usersContainer: state.usersContainer,
  user: state.usersContainer.get('user'),
}};

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(UsersContainer);
