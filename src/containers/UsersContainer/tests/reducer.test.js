
import { fromJS } from 'immutable';
import usersContainerReducer from '../reducer';

describe('usersContainerReducer', () => {
  it('returns the initial state', () => {
    expect(usersContainerReducer(undefined, {})).toEqual(fromJS({}));
  });
});
