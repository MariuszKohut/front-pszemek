/*
 *
 * UsersContainer constants
 *
 */

export const DEFAULT_ACTION = 'UsersContainer/DEFAULT_ACTION';

export const cReducer = {
  loading: 'loading',
  items: 'items',
  user: 'user',
};

export const FETCH_USERS = 'UsersContainer/FETCH_USERS';

export const FETCH_USER = 'UsersContainer/FETCH_USER';

export const SAVE_USER = 'UsersContainer/SAVE_USER';
