/*
 *
 * UsersContainer reducer
 *
 */

import { Map } from 'immutable';
import {
  cReducer as c,
  DEFAULT_ACTION,
  FETCH_USERS,
  FETCH_USER,
  SAVE_USER,
} from './constants';
import { FULFILLED, REJECTED } from '../../global/constants';
import { mapUserListToMap, mapNotes } from '../../models/User';

const initialState = Map({
  loading: true,
  user: new Map(),
});

function userReducer(state = initialState, action) {
  console.log(action.type);
  switch (action.type) {
    case `${FETCH_USER}`:
      return state.set('loading', true);
    case FETCH_USER+'_REJECTED':
      return state;
    case `${FETCH_USER}${FULFILLED}`:
      // Here we map our array of items in response to Map
      // Where each key in map will be same as item ID
      // const user = mapUserListToMap(action.response);
      // console.log(user);
      const forMapping =
        [
        {
            "id": 4,
            "owner": {
                "userId": 1,
                "username": "User",
                "password": "{bcrypt}$2a$10$w/bKAeyjOBxj0r6DdT6fie6.dJHmqPRjhRPzMcx/ELz1wnRFneRKK",
                "enabled": true,
                "credentialsNonExpired": true,
                "accountNonExpired": true,
                "accountNonLocked": true,
                "authorities": null
            },
            "notes": [
                {
                    "id": 1,
                    "content": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris",
                    "title": "aaaa",
                    "durationInDays": 0,
                    "createdOn": [
                        2018,
                        8,
                        31
                    ],
                    "group": null,
                    "tag": null
                },
                {
                    "id": 2,
                    "content": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris",
                    "title": "bbbbb",
                    "durationInDays": 0,
                    "createdOn": [
                        2018,
                        8,
                        31
                    ],
                    "group": null,
                    "tag": null
                },
                {
                    "id": 3,
                    "content": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris",
                    "title": "ccccccccc",
                    "durationInDays": 0,
                    "createdOn": [
                        2018,
                        8,
                        31
                    ],
                    "group": null,
                    "tag": null
                }
            ],
            "tag": null,
            "label": "Awesome group"
        },
        {
            "id": 5,
            "owner": {
                "userId": 2,
                "username": "John",
                "password": "{bcrypt}$2a$10$CXVuPbFqTv2ZswoLQ1HlMOYHv.Ef9t4wiBJcXDUPwPYVjLQkqs95O",
                "enabled": true,
                "credentialsNonExpired": true,
                "accountNonExpired": true,
                "accountNonLocked": true,
                "authorities": null
            },
            "notes": [],
            "tag": null,
            "label": "Mozart group"
        },
        {
            "id": 6,
            "owner": {
                "userId": 3,
                "username": "Bob",
                "password": "{bcrypt}$2a$10$Q8V8a7v/QvCUsj/VYK2Ba.zlYXJgLNUl943056O.WeZRoMjU3Jdh2",
                "enabled": true,
                "credentialsNonExpired": true,
                "accountNonExpired": true,
                "accountNonLocked": true,
                "authorities": null
            },
            "notes": [],
            "tag": null,
            "label": "Circus group"
        }
    ];
      const user = mapNotes(forMapping);
      console.log(user);
      return state.set('loading', false).set('user', user);
      case `${SAVE_USER}`:
        return state.set('loading', true);
      case SAVE_USER+'_REJECTED':
        return state;
      case `${SAVE_USER}${FULFILLED}`:
        // Here we map our array of items in response to Map
        // Where each key in map will be same as item ID
        // const user = mapUserListToMap(action.response);
        // console.log(user);
        return state.set('loading', false);
    default:
      return state;
  }
}

export async function promiseRetarded(src) {
  let res = await src;
  return res.name;
}

export default userReducer;
