/*
 *
 * UsersContainer actions
 *
 */

import {
  DEFAULT_ACTION,
  FETCH_USERS,
  FETCH_USER,
  SAVE_USER,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

export function fetchUsers() {
  return {
    type: FETCH_USERS
  };
}


export function fetchUser() {
  return {
    type: FETCH_USER
  };
}

export function saveUser(data) {
  return {
    type: SAVE_USER,
    payload: data,
  };
}
