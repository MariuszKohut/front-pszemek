/*
*
* UsersContainer saga
*
*/

import { take, call, put, select, takeLatest, all, fork } from 'redux-saga/effects';
import { FETCH_USERS, FETCH_USER, SAVE_USER } from './constants';
import { url, get, post, del, update, setToken, getToken } from '../../network';
import request from '../../request';
import { fulfilled, rejected } from '../../global/actions';
import * as actions from './actions';
import URLSearchParams from 'url-search-params';
import {
  cReducer as c,

} from './constants';
// Individual exports for testing
// export function* fetchUsers(action) {
//   try {
//     // WARNING:
//     // This is hard-coded url for introduction purposes only
//     // For your purposes there is method url() which you will use for getting data from your backend
//     // In your case your urls will look like: url('/users')
//     // You may found more in network.js file, you may configure headers here etc.
//     const url = 'http://localhost:8081/auth/oauth/token';
//     const searchParams = new URLSearchParams();
//       searchParams.set('grant_type', 'password');
//       searchParams.set('username', 'john');
//       searchParams.set('password', '123');
//     const response = yield call(request, url, fetch(url, {
//       method: 'post',
//       headers: new Headers({
//         'Accept': 'application/json',
//         'Content-Type': 'application/x-www-form-urlencoded',
//         'Authorization': 'Basic ' + btoa('frontendClientId:frontendClientSecret'),
//       }),
//       body:  searchParams,
//     }).then(response => response.json()).then(data => {
//       setToken(data.access_token);
//     }));
//
//     yield put(fulfilled(FETCH_USERS, response));
//   } catch (err) {
//     yield put(rejected(FETCH_USERS, err));
//   }
// }

export function* fetchUser(action) {
  try {
    const getUrl = 'http://localhost:8081/auth/user/me';
    const resp = yield call(request, getUrl, get());
    yield put(fulfilled(FETCH_USER, resp));
  } catch (err) {
    yield put(rejected(FETCH_USER, err));
  }
}

export function* saveUser(action) {
  try {
    // WARNING:
    // This is hard-coded url for introduction purposes only
    // For your purposes there is method url() which you will use for getting data from your backend
    // In your case your urls will look like: url('/users')
    // You may found more in network.js file, you may configure headers here etc.
    const getUrl = 'http://localhost:8081/auth/user/me';
    const password = action.payload.get('password');
    const username = action.payload.get('username');
    const response = yield call(request, getUrl, fetch(getUrl, {
      method: 'post',
      body: JSON.stringify({
            password: password,
            username: username,
          })
    }));

    yield put(fulfilled(SAVE_USER, response));
  } catch (err) {
    yield put(rejected(SAVE_USER, err));
  }
}


// All sagas to be loaded
export default function* sagas() {
  yield all([
    // yield takeLatest(FETCH_USERS, action => fetchUsers(action)),
    yield takeLatest(FETCH_USER, action => fetchUser(action)),
    yield takeLatest(SAVE_USER, action => saveUser(action)),
  ]);
}
