import { all } from 'redux-saga/effects';
import IntroductionSaga from './containers/IntroductionContainer/sagas';
import UsersContainterSaga from './containers/UsersContainer/sagas';
import LoginSaga from './containers/Login/sagas';

export default function* rootSaga() {
    yield all([
      // There will be all your container´s sagas
      // 1. Import saga ex. import SomeSaga from './containers/SomeContainer/sagas'
      // 2. Include imported saga in this array.
      // IMPORTANT: It´s function, DO NOT forget to include it as called function, ex. SomeSaga()

      IntroductionSaga(),
      UsersContainterSaga(),
      LoginSaga()
    ])
};
