import rootReducer from '../../reducers';
import rootSaga from '../../sagas';
import { createBrowserHistory } from 'history';
import { routerReducer, routerMiddleware } from 'react-router-redux';
import createSagaMiddleware from 'redux-saga';
import promiseMiddleware from 'redux-promise-middleware';
import { applyMiddleware, combineReducers, createStore } from 'redux';

/**
 * Reducers - Root reducer + router reducer
 * @type {Reducer<S>}
 */
const reducers = combineReducers({
  ...rootReducer,
  router: routerReducer,
});

/**
 * History - used for redirecting
 */
export const history = createBrowserHistory();

/**
 * Store generator
 * @param initialState
 * @returns {{store: Store<S>, runSaga: (function())}}
 */
export const configureStore = () => {
  // Middleware - Saga
  const sagaMiddleware = createSagaMiddleware();

  // Dev Tools setup
  const reduxDevTools =
    window.__REDUX_DEVTOOLS_EXTENSION__ &&
    window.__REDUX_DEVTOOLS_EXTENSION__();

  // Middleware - Others
  const middlewares = [
    promiseMiddleware(),
    sagaMiddleware,
    routerMiddleware(history),
  ];

  // Store - create a store
  const store = createStore(
    reducers,
    reduxDevTools,
    applyMiddleware(...middlewares)
  );

  // Return store and runSaga method to reinitialize sagas
  return {
    store,
    runSaga: () => sagaMiddleware.run(rootSaga),
  };
};
