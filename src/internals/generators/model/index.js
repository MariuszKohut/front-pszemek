/**
 * Container Generator
 */

const componentExists = require('../utils/componentExists');

module.exports = {
  description: 'Add model',
  prompts: [{
    type: 'input',
    name: 'name',
    message: 'What should it be called?',
    default: 'User',
    validate: (value) => {
      if ((/.+/).test(value)) {
        return componentExists(value) ? 'A model with this name already exists' : true;
      }
      return 'The name is required';
    },
  }],
  actions: (data) => {
    // Generate index.js
    return [{
      type: 'add',
      path: '../../models/{{properCase name}}.js',
      templateFile: './model/index.js.hbs',
      abortOnFail: true,
    }];
  },
};
