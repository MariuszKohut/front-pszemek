import React from 'react';
import ReactDOM from 'react-dom';
import registerServiceWorker from './internals/server/registerServiceWorker';
import { configureStore } from './internals/server/store';
import BootstrapStyles from './internals/styles/bootstrap.min.css';
import { routes } from './routes';

// Store configuration
const store = configureStore();
store.runSaga();

// Router
ReactDOM.render(routes(store.store), document.getElementById('root'));

registerServiceWorker();
